# HOW TO "INSTALL"
Clone this repository and create a soft-link for the desired utility in your `$HOME/.local/bin`, which should also be in your PATH environment variable.

```bash
cd cli-utils # or your chosen name for the cloned directory
ln -s $PWD/<command> $HOME/.local/bin/<command>
```

# UTILS
Collection of bash scripts that may be useful.

## coolgrep
Util to ease finding content in files.

### Requirements
This util requires:
- GNU's bash, grep, find, xargs, comm, and cat.
- It also requires [_fzf_](https://github.com/junegunn/fzf). It can be installed from source or with most package managers.

### USAGE
coolgrep requires the following arguments:
- a file pattern filter (used by GNU _find_)
- a _positive_ pattern (used by GNU _grep_)
- [OPT] a _negative_ pattern (used by GNU _grep_)

After a short while, _coolgrep_ will initialize an interactive [_fzf_](https://github.com/junegunn/fzf) piping all content of matching files.
Search through the content and press enter to get files (fuzzy)matching your pre-filtered content.

NOTE: care to filter as much as possible with _find_.

### TODO
- Improve GNU _find_ filtering options GNU _find_
- Somehow add warning limits when content piped to _fzf_ is to large (use _tee_ and _du_!)
- Add optional filtering options to GNU _find_
- Add optional filtering options to GNU _grep_
- Add optional performance options to xargs filtering options to GNU _grep_
- Check if GNU _tee_ can be used to optimize grep when passing the negative parameter options.
- Use [Typer](https://typer.tiangolo.com/) to wrap the utility
